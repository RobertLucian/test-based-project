import json


def getMatrixMinor(m, i, j):
    return [row[:j] + row[j + 1 :] for row in (m[:i] + m[i + 1 :])]


def isSquare(m):
    return all(len(row) == len(m) for row in m)


def getMatrixDeterminant(m):
    """
    Determinant must be a matrix of nxn elements with n > 0 and n <= 3.
    The elements within the matrix must be integers.
    Returns None if the matrix is not square.
    """
    if not isSquare(m):
        return None

    # base case for 1x1 matrix
    if len(m) == 1:
        return m[0][0]
    # base case for 2x2 matrix
    if len(m) == 2:
        return m[0][0] * m[1][1] - m[0][1] * m[1][0]

    # for when the matrix has a n-by-n size, with n > 2
    determinant = 0
    for c in range(len(m)):
        determinant += (
            ((-1) ** c) * m[0][c] * getMatrixDeterminant(getMatrixMinor(m, 0, c))
        )
    return determinant


def app(data):
    matrix = data["matrix"]
    scalar = data["scale"]  # the scaler is a positive natural number

    determinant = getMatrixDeterminant(matrix)
    if determinant is None:
        return None
    else:
        return determinant * scalar


if __name__ == "__main__":

    with open("config.json", "r") as f:
        data = json.load(f)

    result = app(data)
    print(result)
