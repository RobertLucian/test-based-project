# Test-Based Project

## O Short Description

Pentru inceput, input-ul aplicatiei este alcatuit din:

1. O matrice de n-pe-n elemente, unde n-ul apartine setului *{1, 2, 3}*.
1. Un scalar reprezentat de un numar intreg.

Aceste 2 input-uri intra in compozitia output-ului in felul urmator: se calculeaza determinantul matricei, iar apoi rezultatul obtinut se scaleaza cu scalarul dat.

Interpretarea geometrica ar fi ca programul calculeaza lungimea, aria sau volumul (in functie de dimensiunea matricei) in baza vectorilor dati, iar apoi aceasta valoare se scaleaza.

## Equivalence Partition

Pentru clasele de echivalenta, am ajuns la urmatoarele partitii:
1. Atunci cand determinantul este 0.
1. Atunci cand scalarul este 0.
1. Atunci cand matricea nu este patratica.

## Boundary Testing

Pentru analiza valorilor de frontiera, am gasit ca se pot face urmatoarele tipuri de teste:
1. Atunci cand rangul dimensiunea matricei este 1 si 3.
1. Atunci cand scalarul este -10, 0 si 10.

Pentru aceste 2 cazuri, testam pe matrici cu determinantul 0 si/sau scalarul tot 0.

## Category Partition

1. Avem o singura unitate.
1. Parametrii sunt: matricea de intrare (care are un n-pe-n numar de elemente) si un scalar.
1. Categorii: daca matricea este 1,2,3-dimensionala si daca scalarul este intre -10 si 10 (lungime minima, medie si maxima).

## McCabe Complexity

Complexitate McCabe este:
1. **5** pentru `run.getMatrixDeterminant`.
1. **2** pentru `run.app`.
1. **1** pentru `run.getMatrixMinor`.
1. **1** pentru `run.isSquare`.

Aceasta complexitate se regaseste si in grafurile urmatoare:

![](graphs/output.png)

## Mutation Testing

Si aceasta e partea cu testarea mutantilor:

![](mutmut.PNG)

Se observa ca din 51 de mutanti, 6 au supravietuit.

## Testing

```

pylama -l mccabe,pylint mypkg
python -m mccabe --min 1 mypkg/run.py
python -m mccabe --min 1 --dot mypkg/run.py > graph.dot
dot -Tpng -y graphs/graph.dot > graphs/output.png
pytest
mutmut run
```

## Complexity Graph

![](graph.jpg)