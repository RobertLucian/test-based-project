import pytest
from mypkg import run

test_set = [

    ######################
    # EQUIVALENCE TESTING
    #####################

    # determinant is 0
    ({
        'matrix': [[2, 0],[2, 0]],
        'scale': 1
    }, 0),
    ({
        'matrix': [[7, 21],[1, 3]],
        'scale': 1
    }, 0),
    ({
        'matrix': [[12, 4],[9, 3]],
        'scale': 1
    }, 0),
    ({
        'matrix': [[0, -1],[0, 0]],
        'scale': 1
    }, 0),
    ({
        'matrix': [[0, -1],[0, 0]],
        'scale': 4
    }, 0),

    # scalar is 0
    ({
        'matrix': [[6, 2],[2, -3]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[5, 2],[2, 4]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[122]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[0, 2, 81],[2, 0, 42], [1,-3, 71]],
        'scale': 0
    }, 0),({
        'matrix': [[0, 2],[2, 0]],
        'scale': 0
    }, 0),

    # non-square matrix
    ({
        'matrix': [[0, 2],[2, 0],[13,-1]],
        'scale': 1
    }, None),
    ({
        'matrix': [[0, 2]],
        'scale': 2
    }, None),
    ({
        'matrix': [[0], [0]],
        'scale': 1
    }, None),
    ({
        'matrix': [[0], [1], [-1]],
        'scale': 4
    }, None),
    ({
        'matrix': [[-4, 10, 7]],
        'scale': 6
    }, None),
    ({
        'matrix': [[0, 2, 1],[2, 0, 9]],
        'scale': 0
    }, None),

    #####################
    # BOUNDARY TESTING
    ####################

    # 1-st order matrix w/ scale != 0
    ({
        'matrix': [[1]],
        'scale': 1
    }, 1),
    ({
        'matrix': [[4]],
        'scale': 1
    }, 4),
    ({
        'matrix': [[-3]],
        'scale': 1
    }, -3),
    ({
        'matrix': [[311]],
        'scale': 1
    }, 311),
    ({
        'matrix': [[289]],
        'scale': 2
    }, 289),

    # 3-rd order matrix w/ scale != 0
    ({
        'matrix': [[0, 2, 81],[2, 0, 42], [1,-3, 71]],
        'scale': 1
    }, -686),
    ({
        'matrix': [[1, 2, 81],[-1, 0, 0], [1,-4, 13]],
        'scale': 1
    }, 350),
    ({
        'matrix': [[0, 17, 0],[2, 2, 2], [1,-12, 43]],
        'scale': 1
    }, -1428),
    ({
        'matrix': [[0, -19, 0],[-1, 1, 9], [1,-83, 91]],
        'scale': 1
    }, -1900),

    # scale = 0 w/ determinant != 0
    ({
        'matrix': [[1]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[2]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[3]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[4]],
        'scale': 0
    }, 0),

    # scale = 10 w/ determinant != 0
    ({
        'matrix': [[1]],
        'scale': 10
    }, 10),
    ({
        'matrix': [[2]],
        'scale': 10
    }, 20),
    ({
        'matrix': [[3]],
        'scale': 10
    }, 30),
    ({
        'matrix': [[4]],
        'scale': 10
    }, 40),

    # scale = -10 w/ determinant != 0
    ({
        'matrix': [[1]],
        'scale': -10
    }, -10),({
        'matrix': [[2]],
        'scale': -10
    }, -20),
    ({
        'matrix': [[3]],
        'scale': -10
    }, -30),
    ({
        'matrix': [[4]],
        'scale': -10
    }, -40),

    ##################
    # CATEGORY TESTING
    ##################

     # 1-st order matrix w/ scale = -10
    ({
        'matrix': [[1]],
        'scale': -10
    }, -10),
    ({
        'matrix': [[9]],
        'scale': -10
    }, -90),

    # 1-st order matrix w/ scale = 0
    ({
        'matrix': [[1]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[21]],
        'scale': 0
    }, 0),

    # 1-st order matrix w/ scale = 10
    ({
        'matrix': [[-3]],
        'scale': 10
    }, -30),
    ({
        'matrix': [[-1]],
        'scale': 10
    }, -10),

    # 2-nd order matrix w/ scale = -10
    ({
        'matrix': [[0, 3], [0, 0]],
        'scale': -10
    }, 0),
    ({
        'matrix': [[0, 0], [0, 6]],
        'scale': -10
    }, 0),

    # 2-nd order matrix w/ scale = 0
    ({
        'matrix': [[3, 0], [3, 0]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[1, 0], [0, 6]],
        'scale': 0
    }, 0),

    # 2-nd order matrix w/ scale = 10
    ({
        'matrix': [[5, 0], [2, 1]],
        'scale': 10
    }, 50),
    ({
        'matrix': [[2, 33], [1, 0]],
        'scale': 10
    }, -330),

    # 3-rd order matrix w/ scale = -10
    ({
        'matrix': [[0, 2, 3],[2, 0, 42], [1,-3, 71]],
        'scale': -10
    }, 2180),
    ({
        'matrix': [[3, 4, 1],[-1, 0, 0], [1,-4, 0]],
        'scale': -10
    }, -40),

    # 3-rd order matrix w/ scale = 0
    ({
        'matrix': [[1, 7, 20],[2, 2, 2], [0,-90, 11]],
        'scale': 0
    }, 0),
    ({
        'matrix': [[6, -1, 43],[0, 0, 0], [1,-22, 11]],
        'scale': 0
    }, 0),

    # 3-rd order matrix w/ scale = 10
    ({
        'matrix': [[9, 7, 54],[2, 4,42], [1,-12, 74]],
        'scale': 10
    }, 49460),
    ({
        'matrix': [[2, -13, 19],[-1, 1, 9], [1,-83, 171]],
        'scale': 10
    }, 10540),

]

@pytest.mark.parametrize("data,result", test_set)
def test_app(data, result):
    assert run.app(data) == result